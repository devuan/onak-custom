#!/bin/bash
#
# Collect stuff into here.

FILES=(
    /root/onak-howto.txt
    /usr/local/bin/gpgwho
    /usr/lib/cgi-bin/pks/auth
    /usr/lib/cgi-bin/pks/listall
    /var/www/onak/index.html
    /var/www/onak/keyicon.ico
    /var/www/onak/keyring.css
    /etc/nginx/sites-available/keyring.devuan.org
)

ENCS=(
    /var/www/onak/passwd
)

encrypt() {
    openssl enc -aes-256-cbc -salt -a -iter 15342
}

decrypt() {
    openssl enc -aes-256-cbc -d -a -iter 15342
}


case "$1" in
    publish)
	for f in ${FILES[@]} ; do cp -vu ${f##*/} $f ; done
	for f in ${ENCS[@]} ; do decrypt < ${f##*/} > $f ; done
	;;
    encrypt|decrypt)
	$1
	;;
    *)
	for f in ${FILES[@]} ; do cp -vu $f ${f##*/} ; done
	for f in ${ENCS[@]} ; do encrypt < $f > ${f##*/} ; done
	;;
esac
