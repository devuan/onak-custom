onak-custom
===========

This is a collection of customization items for onak to extend the web
access with branding, authenticated update and a "list all"
function. It also includes the nginx web frontend configurtaion file,
and a helper script (collect.sh) to refresh these files from in-place
changes in the current setup, and conversely, to push these files to
their deployment residences.

To pull the currently deployed files to here, use:

    # ./collect.sh

To push the current files into deployment, use:

    # ./collect.sh publish

The "ENCS" files are saved here encrypted with the devuanteam
password, which you must enter when collecting.
